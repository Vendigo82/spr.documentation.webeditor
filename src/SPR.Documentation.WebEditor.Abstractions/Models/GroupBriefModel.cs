﻿using System;
using System.ComponentModel.DataAnnotations;

namespace SPR.Documentation.WebEditor.Models
{
    public class GroupBriefModel
    {
        public Guid Id { get; set; }

        [Required, MinLength(1)]
        public string Alias { get; set; } = null!;
    }
}
