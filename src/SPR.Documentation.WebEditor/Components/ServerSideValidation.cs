﻿using Microsoft.AspNetCore.Components;
using Microsoft.AspNetCore.Components.Forms;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SPR.Documentation.WebEditor.Components
{
    public class ServerSideValidation : ComponentBase
    {
        private ValidationMessageStore messageStore = null!;

        [CascadingParameter] 
        private EditContext? CurrentEditContext { get; set; }

        /// <inheritdoc />
        protected override void OnInitialized()
        {
            if (CurrentEditContext == null)
            {
                throw new InvalidOperationException($"{nameof(ServerSideValidation)} requires a cascading " +
                    $"parameter of type {nameof(EditContext)}. For example, you can use {nameof(ServerSideValidation)} " +
                    $"inside an {nameof(EditForm)}.");
            }

            messageStore = new ValidationMessageStore(CurrentEditContext);
            CurrentEditContext.OnValidationRequested += (s, e) => messageStore.Clear();
            CurrentEditContext.OnFieldChanged += (s, e) => messageStore.Clear(e.FieldIdentifier);
        }

        public void DisplayErrors(Dictionary<string, List<string>> errors) 
            => DisplayErrors(errors.Select(i => new KeyValuePair<string, IEnumerable<string>>(i.Key, i.Value)));
        //{
        //    foreach (var err in errors)
        //    {
        //        messageStore.Add(CurrentEditContext!.Field(err.Key), err.Value);
        //    }
        //    CurrentEditContext!.NotifyValidationStateChanged();
        //}

        public void DisplayErrors(IEnumerable<KeyValuePair<string, IEnumerable<string>>> errors)
        {
            foreach (var err in errors)
            {
                messageStore.Add(CurrentEditContext!.Field(err.Key), err.Value);
            }
            CurrentEditContext!.NotifyValidationStateChanged();
        }

        public void ClearErrors()
        {
            messageStore.Clear();
            CurrentEditContext!.NotifyValidationStateChanged();
        }
    }
}
