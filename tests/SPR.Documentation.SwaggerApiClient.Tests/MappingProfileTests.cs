using AutoMapper;
using System;
using Xunit;

namespace SPR.Documentation.SwaggerApiClient.Tests
{
    public class MappingProfileTests
    {
        [Fact]
        public void ValidateConfigurationTest()
        {
            var cfg = new MapperConfiguration(c => c.AddProfile<MappingProfile>());
            cfg.AssertConfigurationIsValid();
        }
    }
}
