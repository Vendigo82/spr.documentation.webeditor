﻿
using System.ComponentModel.DataAnnotations;

namespace SPR.Documentation.WebEditor.Models
{
    /// <summary>
    /// Function argument
    /// </summary>
    public class ArgumentModel
    {
        /// <summary>
        /// Argument's type
        /// </summary>
        [Required]
        public string Type { get; set; } = null!;

        /// <summary>
        /// Argument's name
        /// </summary>
        [Required]
        public string Name { get; set; } = null!;
    }
}
