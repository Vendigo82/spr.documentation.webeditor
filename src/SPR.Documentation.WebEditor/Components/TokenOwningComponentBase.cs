﻿using Microsoft.AspNetCore.Components;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using SPR.Documentation.WebEditor.Services;
using System.Threading.Tasks;

namespace SPR.Documentation.WebEditor.Components
{
    /// <summary>
    /// Copy access token's from SignalR scope to component's scope
    /// </summary>
    /// <typeparam name="TService"></typeparam>
    public class TokenOwningComponentBase<TService> : OwningComponentBase<TService> where TService : notnull
    {
        [Inject]
        private ITokenContainer TokenProvider { get; set; } = null!;

        //[Inject]
        //private ILogger<TokenOwningComponentBase<TService>> Logger { get; set; } = null!;

        public TokenOwningComponentBase()
        {            
        }

        public override Task SetParametersAsync(ParameterView parameters)
        {
            var scopedTokenProvider = ScopedServices.GetRequiredService<ITokenContainer>();

            //Logger.LogTrace("scopedTokenProvider {@Token}", scopedTokenProvider);

            //scopedTokenProvider.AccessToken = TokenProvider.AccessToken;
            //scopedTokenProvider.RefreshToken = TokenProvider.RefreshToken;
            if (TokenProvider.Token != null)
            {
                scopedTokenProvider.Assign(TokenProvider.Token);
                //Logger.LogDebug("token was assigned {@Token}", scopedTokenProvider);
            } else
            {
                //Logger.LogWarning("TokenProvider is null");
            }

            return base.SetParametersAsync(parameters);
        }

        //protected override void OnInitialized()
        //{
        //    base.OnInitialized();
        //}
    }
}
