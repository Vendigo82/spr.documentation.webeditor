﻿using System;
using System.ComponentModel.DataAnnotations;

namespace SPR.Documentation.WebEditor.Settings
{
    public class IdentityServerSettings
    {
        [Required]
        public Uri BaseUrl { get; set; } = null!;

        [Required]
        public string Authority { get; set; } = null!;

        public bool RequireHttpsMetadata { get; set; } = true;

        public string? ClientSecret { get; set; }

        public DiscoveryPolicySettings DiscoveryPolicy { get; set; } = new DiscoveryPolicySettings();
    }

    public class DiscoveryPolicySettings
    {
        public bool RequireHttps { get; set; } = true;
    }
}
