using HealthChecks.UI.Client;
using IdentityModel.Client;
using IdentityModel;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Components;
using Microsoft.AspNetCore.Diagnostics.HealthChecks;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Mvc.Authorization;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using SPR.Documentation.WebEditor.Services;
using SPR.Documentation.WebEditor.Settings;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Components.Authorization;
using System.Reflection;

namespace SPR.Documentation.WebEditor
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        // For more information on how to configure your application, visit https://go.microsoft.com/fwlink/?LinkID=398940
        public void ConfigureServices(IServiceCollection services)
        {
            const string ISHttpClient = "is-client";

            //services.AddControllers();
            services.AddRazorPages();
            services.AddServerSideBlazor();
            services.AddBootstrapBlazor();

            var identityServerSettings = Configuration.GetSection("IdentityServer").Get<IdentityServerSettings>();
            var documentationApiSettings = Configuration.GetSection("DocumentationApi").Get<EndpointSettings>();

            services.AddHttpContextAccessor();
            //services.AddAccessTokenManagement();//.ConfigureBackchannelHttpClient;

            AddIdentityServer();
            AddHealthChecks();

            services.AddAutoMapper(typeof(SwaggerApiClient.MappingProfile));

            services.AddHttpClient<SwaggerApiClient.IEditorClient, SwaggerApiClient.EditorClient>("ApiClient", client => {
                client.BaseAddress = new Uri(documentationApiSettings.BaseUrl);
            });
            //services.AddTypedClient()
            //services.AddRefitClient<Core.ApiClient.IApiClient>();
            //services.AddHttpClient<SwaggerApiClient.IFunctionsClient, SwaggerApiClient.FunctionsClient>("ApiClient");

            services
                .AddHttpClient(ISHttpClient, client => {
                    client.BaseAddress = identityServerSettings.BaseUrl;
                })
                .AddLoggingHandler();

            //services.AddTransient<AccessTokenDelegatingHandler>();
            services.AddScoped<TokenProvider>();
            services.AddScoped<SwaggerApiClient.ITokenProvider>(sp => sp.GetRequiredService<TokenProvider>());
            services.AddScoped<ITokenContainer>(sp => sp.GetRequiredService<TokenProvider>());
           // var list = services.Where(i => i.ServiceType == typeof(SwaggerApiClient.IEditorClient)).ToList();

            services.AddScoped<Abstractions.IFunctionsService, SwaggerApiClient.SwaggerFunctionsService>();
            services.AddTransient<Abstractions.IDictionarySource, SwaggerApiClient.SwaggerDictionarySource>();
            services.AddTransient<Abstractions.IGroupsService, SwaggerApiClient.SwaggerGroupsService>();

            services.AddTransient<Pages.Functions.FunctionEditForm.ServiceBunch>();

            void AddIdentityServer()
            {
                // https://stackoverflow.com/questions/57065327/blazorserver-side-with-identityserver4
                // https://stackoverflow.com/questions/57172840/how-do-you-create-a-api-identityserver-blazorserver-side-application

                // add openid connect. Is equals to standard web-site application
                services
                    .AddAuthentication(options => {
                        options.DefaultScheme = "Cookies";
                        options.DefaultChallengeScheme = "oidc";
                    })
                    .AddCookie("Cookies", options => {
                        options.Cookie.SecurePolicy = Microsoft.AspNetCore.Http.CookieSecurePolicy.Always;
                        options.Cookie.SameSite = Microsoft.AspNetCore.Http.SameSiteMode.None;
                    })
                    .AddOpenIdConnect("oidc", options => {
                        options.Authority = identityServerSettings.Authority;
                        options.RequireHttpsMetadata = identityServerSettings.RequireHttpsMetadata;

                        options.ClientId = "spr.documentation";
                        options.ClientSecret = identityServerSettings.ClientSecret ?? "meripoppins";
                        options.ResponseType = "code";

                        options.SaveTokens = true;
                        //options.UseTokenLifetime = true;

                        options.TokenValidationParameters.ClockSkew = TimeSpan.FromSeconds(0);

                        options.Scope.Add("openid");
                        options.Scope.Add("profile");
                        options.Scope.Add("offline_access");
                        options.Scope.Add("sprdoc.editor");
                    });


                // Now it should say "Not authenticated" when you open the main page but there's still no redirection to the IdentityServer4.
                // For this you've got to add MVC in the startup too
                services.AddMvcCore(options => {
                    var policy = new AuthorizationPolicyBuilder()
                        .RequireAuthenticatedUser()
                        .Build();
                    options.Filters.Add(new AuthorizeFilter(policy));
                });

                // refresh token's dependencies
                services.AddSingleton<IDiscoveryCache>(sp => {
                    var logger = sp.GetRequiredService < ILogger<Startup>>();
                    var factory = sp.GetRequiredService<IHttpClientFactory>();
                    var discoveryPolicy = new DiscoveryPolicy {
                        RequireHttps = identityServerSettings.DiscoveryPolicy.RequireHttps
                    };
                    logger.LogDebug("Create DiscoveryCache for authority '{Authority}' with policy: {@Policy}", identityServerSettings.Authority, discoveryPolicy);
                    return new DiscoveryCache(identityServerSettings.Authority, () => factory.CreateClient(ISHttpClient), discoveryPolicy);
                });
            }

            void AddHealthChecks()
            {
                services
                    .AddHealthChecks()
                    .AddIdentityServer(new Uri(identityServerSettings.Authority), name: "IdentityServer", tags: new[] { "deep" })
                    .AddUrlGroup(new Uri(new Uri(documentationApiSettings.BaseUrl), documentationApiSettings.Health), name: "Api", tags: new[] { "deep" })
                    ;
            }
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseExceptionHandler("/Error");
                // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
                app.UseHsts();
            }

            app.UseHealthChecks("/health", new HealthCheckOptions {
                ResponseWriter = UIResponseWriter.WriteHealthCheckUIResponse
            });
            app.UseHealthChecks("/health/lite", new HealthCheckOptions {
                ResponseWriter = UIResponseWriter.WriteHealthCheckUIResponse,
                Predicate = (c) => !c.Tags.Contains("deep")
            });

            app.UseHttpsRedirection();
            app.UseStaticFiles();

            app.UseWhen(c => c.Request.Path.Value == "/version", b => b.Run(async context => {
                //var version = System.Reflection.Assembly.GetExecutingAssembly().GetName().Version!.ToString();
                var version = (Assembly.GetExecutingAssembly().GetCustomAttribute(typeof(AssemblyInformationalVersionAttribute)) as AssemblyInformationalVersionAttribute)?.InformationalVersion;
                await context.Response.WriteAsJsonAsync(new { version });
            }));

            app.UseRouting();
            app.UseAuthentication();
            app.UseAuthorization();

            app.UseEndpoints(endpoints => {
                endpoints.MapBlazorHub().RequireAuthorization();
                endpoints.MapFallbackToPage("/_Host");
            });
        }
    }
}
