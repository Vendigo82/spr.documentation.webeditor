﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace SPR.Documentation.WebEditor.Exceptions
{
    public class ModelValidationException : Exception
    {
        public ModelValidationException(IEnumerable<KeyValuePair<string, IEnumerable<string>>> validationErrors)
        {
            ValidationErrors = validationErrors ?? throw new ArgumentNullException(nameof(validationErrors));
        }

        public ModelValidationException(string key, string error) 
            : this(Enumerable.Repeat(KeyValuePair.Create(key, Enumerable.Repeat(error, 1)), 1))
        {
        }

        public IEnumerable<KeyValuePair<string, IEnumerable<string>>> ValidationErrors { get; }

    }
}
