﻿//using System;
//using System.Collections.Generic;
//using System.Linq;
//using System.Net.Http;
//using System.Threading;
//using System.Threading.Tasks;

//namespace SPR.Documentation.WebEditor.Services
//{
//    public class AccessTokenDelegatingHandler : DelegatingHandler
//    {
//        private readonly IAccessTokenStorage tokenStorage;

//        public AccessTokenDelegatingHandler(IAccessTokenStorage tokenStorage)
//        {
//            this.tokenStorage = tokenStorage ?? throw new ArgumentNullException(nameof(tokenStorage));
//        }

//        protected override Task<HttpResponseMessage> SendAsync(HttpRequestMessage request, CancellationToken cancellationToken)
//        {
//            if (tokenStorage.T.AccessToken != null)
//                request.Headers.Add("Authorization", $"Bearer {tokenStorage.AccessToken}");

//            return base.SendAsync(request, cancellationToken);
//        }
//    }
//}
