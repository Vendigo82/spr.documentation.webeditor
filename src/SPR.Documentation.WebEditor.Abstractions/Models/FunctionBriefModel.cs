﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SPR.Documentation.WebEditor.Models
{
    /// <summary>
    /// Brief information about function
    /// </summary>
    public class FunctionBriefModel
    {
        public Guid Id { get; set; }

        [Required, MinLength(1)]
        [Display(Name = "function name")]
        public string SystemName { get; set; } = null!;

        [Required, Range(0, 10)]
        [Display(Name = "arguments count")]
        public int ArgsCount { get; set; }
    }
}
