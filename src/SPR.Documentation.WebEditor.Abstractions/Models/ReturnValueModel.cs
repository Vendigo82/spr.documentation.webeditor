﻿
using System.ComponentModel.DataAnnotations;

namespace SPR.Documentation.WebEditor.Models
{
    /// <summary>
    /// Function return's value model
    /// </summary>
    public class ReturnValueModel
    {
        /// <summary>
        /// Function return's type
        /// </summary>
        [Required]
        public string Type { get; set; } = null!;
    }
}
