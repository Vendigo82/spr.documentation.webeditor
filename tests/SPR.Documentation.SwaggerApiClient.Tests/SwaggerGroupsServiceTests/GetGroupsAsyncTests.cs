﻿using AutoFixture.Xunit2;
using FluentAssertions;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace SPR.Documentation.SwaggerApiClient.SwaggerGroupsServiceTests
{
    public class GetGroupsAsyncTests : SwaggerGroupsServiceBaseTests
    {
        [Theory, AutoData]
        public async Task GetGroups_ShouldSuccess(GroupBriefModelItemsContainer response)
        {
            // setup
            clientMock.Setup(f => f.EditorGroupsGetAsync(default)).ReturnsAsync(response);

            // action
            var result = await target.GetGroupsAsync();

            // asserts
            result.Should().BeEquivalentTo(response.Items);
        }
    }
}
