﻿using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SPR.Documentation.WebEditor.Pages
{
    public class _HostAuthModel : PageModel
    {
        public IActionResult OnGetLogin([FromQuery(Name = RoutingConsts.RedirectUrlQueryParam)] string? redirectUrl)
        {
            return Challenge(AuthProps(redirectUrl), "oidc");
        }

        public async Task OnGetLogoutRoutine()
        {
            await HttpContext.SignOutAsync("Cookies");
            await HttpContext.SignOutAsync("oidc", AuthProps());
        }

        private AuthenticationProperties AuthProps(string? redirectUrl = null) => new()
        {
            RedirectUri = Url.Content($"~/{redirectUrl ?? string.Empty}")
        };
    }
}
