﻿using AutoFixture.Xunit2;
using FluentAssertions;
using Microsoft.AspNetCore.Http;
using Moq;
using SPR.Documentation.WebEditor.Exceptions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace SPR.Documentation.SwaggerApiClient.Tests.SwaggerFunctionsServiceTests
{
    public class FunctionNewAsyncTests : SwaggerFunctionsServiceBaseTests
    {
        [Theory, AutoData]
        public async Task SuccessTest(WebEditor.Models.FunctionModel model, IdModel response)
        {
            // setup
            clientMock.Setup(f => f.EditorFunctionsPutAsync(It.IsAny<FunctionModel>(), default)).ReturnsAsync(response);

            // action
            var result = await target.FunctionNewAsync(model);

            // asserts
            var args = new List<FunctionModel>();
            clientMock.Verify(f => f.EditorFunctionsPutAsync(Capture.In(args), default), Times.Once);
            clientMock.VerifyNoOtherCalls();

            args.Should().ContainSingle().Which.Should().BeEquivalentTo(model);
            result.Should().Be(response.Id);
        }

        [Theory, AutoData]
        public async Task FunctionNew_RespondUnprocessableEntity_ShouldBeModelValidationException(WebEditor.Models.FunctionModel model, ValidationProblemDetails payload)
        {
            // setup
            clientMock.Setup(f => f.EditorFunctionsPutAsync(It.IsAny<FunctionModel>(), default))
                .ThrowsAsync(new ApiException<ValidationProblemDetails>("", StatusCodes.Status422UnprocessableEntity, "", null, payload, null));

            // action
            Func<Task> action = () => target.FunctionNewAsync(model);

            // asserts
            var e = (await action.Should().ThrowExactlyAsync<ModelValidationException>()).Which
                .ValidationErrors.Should().BeEquivalentTo(payload.Errors);            
        }
    }
}
