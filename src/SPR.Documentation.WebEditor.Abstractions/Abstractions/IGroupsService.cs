﻿using SPR.Documentation.WebEditor.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SPR.Documentation.WebEditor.Abstractions
{
    public interface IGroupsService
    {
        /// <summary>
        /// Get list of groups
        /// </summary>
        /// <returns></returns>
        Task<IEnumerable<GroupBriefModel>> GetGroupsAsync();

        /// <summary>
        /// Get group by id
        /// </summary>
        /// <param name="id"></param>
        /// <returns>Null if not found</returns>
        Task<GroupModel?> GetGroupAsync(Guid id);

        /// <summary>
        /// Create new group
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        Task<Guid> CreateGroupAsync(GroupModel model);

        /// <summary>
        /// Update existed group
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        Task UpdateGroupAsync(GroupModel model);
    }
}
