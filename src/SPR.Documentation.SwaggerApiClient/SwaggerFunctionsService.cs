﻿using AutoMapper;
using Microsoft.AspNetCore.Http;
using SPR.Documentation.WebEditor.Abstractions;
using SPR.Documentation.WebEditor.Exceptions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SPR.Documentation.SwaggerApiClient
{
    public class SwaggerFunctionsService : IFunctionsService
    {
        private readonly IEditorClient _client;
        private readonly IMapper _mapper;

        public SwaggerFunctionsService(IEditorClient client, IMapper mapper)
        {
            _client = client ?? throw new ArgumentNullException(nameof(client));
            _mapper = mapper ?? throw new ArgumentNullException(nameof(mapper));           
        }

        public async Task<(IEnumerable<WebEditor.Models.FunctionBriefModel> items, string? nextToken)> GetListAsync(string? nextToken, string? filter, int? maxCount)
        {           
            var response = await _client.EditorFunctionsGetAsync(filter: filter, maxCount: maxCount, token: nextToken);
            var result = _mapper.Map<IEnumerable<WebEditor.Models.FunctionBriefModel>>(response.Items);
            return (result, response.NextToken);
        }

        public async Task<(IEnumerable<WebEditor.Models.FunctionBriefModel> items, bool exhausted)> GetListAsync(string? filter, int? offset, int? maxCount)
        {
            var response = await _client.EditorFunctionsGetAsync(filter: filter, offset: offset, maxCount: maxCount);
            var result = _mapper.Map<IEnumerable<WebEditor.Models.FunctionBriefModel>>(response.Items);
            return (result, response.Exhausted);
        }

        public async Task<int> GetCountAsync(string? filter)
        {
            var response = await _client.EditorFunctionsCountAsync(filter);
            return response.Count;
        }

        public async Task<WebEditor.Models.FunctionModel?> GetFunctionAsync(Guid functionId)
        {
            try
            {
                var response = await _client.EditorFunctionsGetAsync(functionId);
                var result = _mapper.Map<WebEditor.Models.FunctionModel>(response);
                return result;
            }
            catch (ApiException e) when (e.StatusCode == StatusCodes.Status204NoContent)
            {
                return null;
            }
        }

        public async Task FunctionUpdateAsync(WebEditor.Models.FunctionModel model)
        {
            var request = _mapper.Map<FunctionModel>(model);
            try
            {
                await _client.EditorFunctionsPostAsync(model.Id, request);
            } catch (ApiException<ValidationProblemDetails> e) when (e.StatusCode == StatusCodes.Status422UnprocessableEntity && e.Result.Errors != null)
            {
                throw e.ToValidationException();
            }
            catch (ApiException e) when (e.StatusCode == StatusCodes.Status409Conflict)
            {
                throw new ModelValidationException(nameof(model.SystemName), e.Message);
            }
        }

        public async Task<Guid> FunctionNewAsync(WebEditor.Models.FunctionModel model)
        {
            var request = _mapper.Map<FunctionModel>(model);
            try
            {
                var response = await _client.EditorFunctionsPutAsync(request);
                return response.Id;
            } 
            catch (ApiException<ValidationProblemDetails> e) when (e.StatusCode == StatusCodes.Status422UnprocessableEntity && e.Result.Errors != null)
            {
                throw e.ToValidationException();
            }
            catch (ApiException<ProblemDetails> e) when (e.StatusCode == StatusCodes.Status409Conflict && e.Result.Title == "Function already exists")
            {
                throw new ModelValidationException(nameof(model.SystemName), e.Result.Detail!);
            }
            catch (ApiException<ProblemDetails> e) when (e.StatusCode == StatusCodes.Status409Conflict && e.Result.Title == "Primary key conflict")
            {
                return model.Id;
            }
        }
    }
}
