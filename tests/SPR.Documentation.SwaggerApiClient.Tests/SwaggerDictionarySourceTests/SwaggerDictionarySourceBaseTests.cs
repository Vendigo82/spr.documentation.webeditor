﻿using Moq;
using SPR.Documentation.WebEditor.Abstractions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SPR.Documentation.SwaggerApiClient.SwaggerDictionarySourceTests
{
    public class SwaggerDictionarySourceBaseTests
    {
        protected readonly SwaggerDictionarySource target;
        protected readonly Mock<IEditorClient> clientMock = new();

        public SwaggerDictionarySourceBaseTests()
        {
            target = new(clientMock.Object);
        }
    }
}
