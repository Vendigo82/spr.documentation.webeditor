﻿using AutoMapper;

namespace SPR.Documentation.SwaggerApiClient
{
    public class MappingProfile : Profile
    {
        public MappingProfile()
        {
            CreateMap<FunctionBriefModel, WebEditor.Models.FunctionBriefModel>().ReverseMap();
            CreateMap<ArgumentModel, WebEditor.Models.ArgumentModel>().ReverseMap();
            CreateMap<ReturnValueModel, WebEditor.Models.ReturnValueModel>().ReverseMap();
            CreateMap<TextResource, WebEditor.Models.TextResource>().ReverseMap();
            CreateMap<FunctionModel, WebEditor.Models.FunctionModel>().ReverseMap();

            CreateMap<GroupBriefModel, WebEditor.Models.GroupBriefModel>();
            CreateMap<GroupModel, WebEditor.Models.GroupModel>().ReverseMap();
        }
    }
}
