﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace SPR.Documentation.SwaggerApiClient
{
    public class ClientBase
    {
        private readonly ITokenProvider tokenProvider;

        public ClientBase(ITokenProvider tokenProvider)
        {
            this.tokenProvider = tokenProvider ?? throw new ArgumentNullException(nameof(tokenProvider));
        }

        public async Task<HttpRequestMessage> CreateHttpRequestMessageAsync(CancellationToken cancellationToken)
        {
            var request = new HttpRequestMessage();
            var token = await tokenProvider.GetAccessTokenAsync();
            request.Headers.Add("Authorization", $"Bearer {token}");
            return request;
        }
    }
}
