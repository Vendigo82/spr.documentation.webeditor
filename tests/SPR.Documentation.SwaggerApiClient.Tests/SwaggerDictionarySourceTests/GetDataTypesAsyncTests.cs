﻿using AutoFixture.Xunit2;
using FluentAssertions;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace SPR.Documentation.SwaggerApiClient.SwaggerDictionarySourceTests
{
    public class GetDataTypesAsyncTests : SwaggerDictionarySourceBaseTests
    {
        [Theory, AutoData]
        public async Task ShouldSuccessTest(StringItemsContainer response)
        {
            // setup
            clientMock.Setup(f => f.EditorDatatypesAsync(default)).ReturnsAsync(response);

            // action
            var result = await target.GetDataTypesAsync();

            // asserts
            result.Should().BeEquivalentTo(response.Items);
        }
    }
}
