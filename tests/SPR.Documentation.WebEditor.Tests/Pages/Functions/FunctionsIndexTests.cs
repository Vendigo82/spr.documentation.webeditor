﻿using AutoFixture.Xunit2;
using Bunit;
using FluentAssertions;
using Microsoft.Extensions.DependencyInjection;
using Moq;
using SPR.Documentation.WebEditor.Abstractions;
using SPR.Documentation.WebEditor.Models;
using SPR.Documentation.WebEditor.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace SPR.Documentation.WebEditor.Pages.Functions
{
    public class FunctionsIndexTests : TestContext
    {
        readonly Mock<IFunctionsService> serviceMock = new();
        readonly Mock<ITokenContainer> tokenContainerMock = new();

        public FunctionsIndexTests()
        {
            Services.AddSingleton(serviceMock.Object);
            Services.AddSingleton(tokenContainerMock.Object);
        }

        [Theory, AutoData]
        public void InitialStateShouldDisplayFunctionsTest(IEnumerable<FunctionBriefModel> items)
        {
            // setup
            serviceMock.Setup(f => f.GetListAsync((string)null, 0, It.IsAny<int?>())).ReturnsAsync((items, true));
            serviceMock.Setup(f => f.GetCountAsync(null)).ReturnsAsync(items.Count);

            // action
            var cut = RenderComponent<FunctionsIndex>();

            // asserts            
            var refs = cut.Find("table").GetElementsByTagName("a").ToList();
            refs.Select(i => i.GetAttribute("href")).Should().Equal(items.Select(i => $"/functions/edit/{i.Id}"));
            refs.Select(i => i.InnerHtml).Should().Equal(items.Select(i => $"{i.SystemName} ({i.ArgsCount})"));

            serviceMock.Verify(f => f.GetListAsync((string)null, 0, It.IsAny<int?>()), Times.Once);
            serviceMock.Verify(f => f.GetCountAsync(null), Times.Once);
            serviceMock.VerifyNoOtherCalls();
        }        

        [Fact]
        public async Task InitialLoadingTextShouldDissapear()
        {
            // setup
            serviceMock.Setup(f => f.GetListAsync((string)null, 0, It.IsAny<int?>())).ReturnsAsync((Enumerable.Empty<FunctionBriefModel>(), true), TimeSpan.FromMilliseconds(100));

            // action
            var cut = RenderComponent<FunctionsIndex>();

            // asserts
            cut.Markup.Should().Contain("Loading");
            await Task.Delay(200);
            cut.Markup.Should().NotContain("Loading");
        }

        [Theory, AutoData]
        public void FilterFunctionsTest(string filter, IEnumerable<FunctionBriefModel> items)
        {
            // setup
            serviceMock.Setup(f => f.GetListAsync(filter, 0, It.IsAny<int?>())).ReturnsAsync((items, true));
            serviceMock.Setup(f => f.GetCountAsync(filter)).ReturnsAsync(items.Count);

            var cut = RenderComponent<FunctionsIndex>();

            // action
            cut.Find("input").Input(filter);
            //cut.
            cut.Find("button").Click();

            // asserts
            var refs = cut.Find("table").GetElementsByTagName("a").ToList();
            refs.Select(i => i.GetAttribute("href")).Should().Equal(items.Select(i => $"/functions/edit/{i.Id}"));
            refs.Select(i => i.InnerHtml).Should().Equal(items.Select(i => $"{i.SystemName} ({i.ArgsCount})"));

            // assert there was initial load
            serviceMock.Verify(f => f.GetListAsync((string)null, 0, It.IsAny<int?>()), Times.Once);
            serviceMock.Verify(f => f.GetCountAsync(null), Times.Once);

            // assert there was loading with filter
            serviceMock.Verify(f => f.GetListAsync(filter, 0, It.IsAny<int?>()), Times.Once);
            serviceMock.Verify(f => f.GetCountAsync(filter), Times.Once);
            serviceMock.VerifyNoOtherCalls();

        }
    }
}
