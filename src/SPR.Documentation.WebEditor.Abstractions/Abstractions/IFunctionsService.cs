﻿using SPR.Documentation.WebEditor.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SPR.Documentation.WebEditor.Abstractions
{
    public interface IFunctionsService
    {
        /// <summary>
        /// Returns bunch of functions
        /// </summary>
        /// <param name="nextToken"></param>
        /// <param name="filter"></param>
        /// <param name="maxCount"></param>
        /// <returns></returns>
        Task<(IEnumerable<FunctionBriefModel> items, string? nextToken)> GetListAsync(string? nextToken, string? filter, int? maxCount);

        /// <summary>
        /// Returns bunch of functions
        /// </summary>
        /// <param name="filter"></param>
        /// <param name="offset"></param>
        /// <param name="maxCount"></param>
        /// <returns></returns>
        Task<(IEnumerable<FunctionBriefModel> items, bool exhausted)> GetListAsync(string? filter, int? offset, int? maxCount);

        /// <summary>
        /// Get total count
        /// </summary>
        /// <param name="filter"></param>
        /// <returns></returns>
        Task<int> GetCountAsync(string? filter);

        /// <summary>
        /// Load function by id
        /// </summary>
        /// <param name="functionId"></param>
        /// <returns>Function's data model or null if not found</returns>
        Task<FunctionModel?> GetFunctionAsync(Guid functionId);

        /// <summary>
        /// Create new function
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        Task<Guid> FunctionNewAsync(FunctionModel model);

        /// <summary>
        /// Update function
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        Task FunctionUpdateAsync(FunctionModel model);
    }
}
