﻿using AutoFixture.Xunit2;
using BootstrapBlazor.Components;
using Bunit;
using FluentAssertions;
using FluentAssertions.BUnit;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Localization;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Moq;
using SPR.Documentation.WebEditor.Abstractions;
using SPR.Documentation.WebEditor.Components;
using SPR.Documentation.WebEditor.Models;
using SPR.Documentation.WebEditor.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace SPR.Documentation.WebEditor.Pages.Groups
{
    public class GroupEditTests : TestContext
    {
        readonly Mock<IGroupsService> serviceMock = new();
        readonly Mock<ITokenContainer> tokenContainerMock = new();
        readonly Mock<MessageService> messageServiceMock;

        public GroupEditTests()
        {
            Services.AddSingleton(serviceMock.Object);
            Services.AddSingleton(tokenContainerMock.Object);
            messageServiceMock = new Mock<MessageService>(() => new MessageService(Mock.Of<IOptionsMonitor<BootstrapBlazorOptions>>(), Mock.Of<IStringLocalizer<MessageService>>()));
            Services.AddSingleton(messageServiceMock.Object);
            Services.AddSingleton(Mock.Of<ILogger<GroupEdit>>());
        }

        [Theory, AutoData]
        public void InitialLoadGroup_ShouldBeSuccess(GroupModel group)
        {
            // setup
            serviceMock.Setup(f => f.GetGroupAsync(group.Id)).ReturnsAsync(group);

            // action
            var cut = RenderComponent<GroupEdit>(ComponentParameter.CreateParameter("Id", group.Id.ToString()));

            // asserts
            cut.Find($"#{nameof(group.Alias)}").GetAttribute("value").Should().Be(group.Alias);
            
            var titles = cut.FindComponent<TextResourcesList>();
            titles.Instance.Items.Should().BeSameAs(group.Titles);
        } 

        [Fact]
        public void InitialLoadNewGroup_ShouldBeSuccess()
        {
            // setup

            // action
            var cut = RenderComponent<GroupEdit>();

            // asserts
            cut.Find($"#{nameof(GroupModel.Alias)}").GetAttribute("value").Should().BeNullOrEmpty();            

            var titles = cut.FindComponent<TextResourcesList>();
            titles.Instance.Items.Should()
                .ContainSingle().Which.Should()
                .BeEquivalentTo(new TextResource { Lang = "ru", Text = string.Empty });
        }

        [Theory, AutoData]
        public void GroupNotFound_ShouldNotFound(Guid groupId)
        {
            // setup
            serviceMock.Setup(f => f.GetGroupAsync(groupId)).ReturnsAsync((GroupModel)null);

            // action
            var cut = RenderComponent<GroupEdit>(ComponentParameter.CreateParameter("Id", groupId.ToString()));

            // asserts
            cut.Markup.Should().Contain("group not found");
        }

        [Theory, AutoData]
        public void SaveNewGroupTest(Guid newGroupId)
        {
            // setup            
            serviceMock.Setup(f => f.CreateGroupAsync(It.IsAny<GroupModel>())).ReturnsAsync(newGroupId);
            var cut = RenderComponent<GroupEdit>();

            // action
            cut.Find($"#{nameof(GroupModel.Alias)}").Change("newvalue");
            cut.Find("form").Submit();

            // asserts
            var args = new List<GroupModel>();
            serviceMock.Verify(f => f.CreateGroupAsync(Capture.In(args)), Times.Once);
            args.Should().ContainSingle().Which.Should().BeEquivalentTo(new {
                Alias = "newvalue"
            });            

            messageServiceMock.Verify(f => f.Show(It.Is<MessageOption>(o => o.Color == Color.Success && o.Content == "Group was saved")));
        }

        [Theory, AutoData]
        public void UpdateGroupTest(GroupModel model)
        {
            // setup            
            serviceMock.Setup(f => f.GetGroupAsync(model.Id)).ReturnsAsync(model);

            var cut = RenderComponent<GroupEdit>(ComponentParameter.CreateParameter(nameof(GroupEdit.Id), model.Id.ToString()));

            // action
            cut.Find($"#{nameof(GroupModel.Alias)}").Change("newvalue");
            cut.Find("form").Submit();

            // asserts
            serviceMock.Verify(f => f.UpdateGroupAsync(model), Times.Once);

            messageServiceMock.Verify(f => f.Show(It.Is<MessageOption>(o => o.Color == Color.Success && o.Content == "Group was saved")));
        }
    }
}
