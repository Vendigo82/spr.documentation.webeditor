﻿using Microsoft.AspNetCore.Components.Forms;
using System.Linq;

namespace SPR.Documentation.WebEditor.Components
{
    public class FunctionsEditFormBootstrapCssClassProvider : BootstrapFieldCssClassProvider
    {
        public override string GetFieldCssClass(EditContext editContext, in FieldIdentifier fieldIdentifier)
        {
            if (fieldIdentifier.FieldName == "withReturnValue" || fieldIdentifier.FieldName == "Selected")
            {
                var isValid = !editContext.GetValidationMessages(fieldIdentifier).Any();
                var isModified = editContext.IsModified(fieldIdentifier);

                return (isModified, isValid) switch {
                    (true, true) => "form-check-input modified is-valid",
                    (true, false) => "form-check-input modified is-invalid",
                    (false, true) => "form-check-input",
                    (false, false) => "form-check-input"
                };
            }
            else
                return base.GetFieldCssClass(editContext, fieldIdentifier);
        }
    }
}
