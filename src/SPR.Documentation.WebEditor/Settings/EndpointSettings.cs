﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SPR.Documentation.WebEditor.Settings
{
    public class EndpointSettings
    {
        public string BaseUrl { get; set; } = null!;

        public string Health { get; set; } = null!;
    }
}
