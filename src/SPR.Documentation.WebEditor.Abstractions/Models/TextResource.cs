﻿
using System.ComponentModel.DataAnnotations;

namespace SPR.Documentation.WebEditor.Models
{
    /// <summary>
    /// Text record
    /// </summary>
    public class TextResource
    {
        /// <summary>
        /// Text record's language
        /// </summary>
        [Required]
        public string Lang { get; set; } = null!;

        /// <summary>
        /// Text
        /// </summary>
        [Required]
        public string Text { get; set; } = null!;
    }
}
