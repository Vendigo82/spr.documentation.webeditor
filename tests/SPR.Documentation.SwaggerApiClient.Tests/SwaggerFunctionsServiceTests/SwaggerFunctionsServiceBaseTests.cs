﻿using AutoMapper;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SPR.Documentation.SwaggerApiClient.Tests.SwaggerFunctionsServiceTests
{
    public class SwaggerFunctionsServiceBaseTests
    {
        protected readonly SwaggerFunctionsService target;
        protected readonly Mock<IEditorClient> clientMock = new();

        public SwaggerFunctionsServiceBaseTests()
        {
            var mapper = new Mapper(new MapperConfiguration(c => c.AddProfile<MappingProfile>()));
            target = new SwaggerFunctionsService(clientMock.Object, mapper);
        }
    }
}
