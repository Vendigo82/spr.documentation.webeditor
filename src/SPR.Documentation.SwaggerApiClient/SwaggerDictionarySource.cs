﻿using SPR.Documentation.WebEditor.Abstractions;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace SPR.Documentation.SwaggerApiClient
{
    public class SwaggerDictionarySource : IDictionarySource
    {
        private readonly IEditorClient client;

        public SwaggerDictionarySource(IEditorClient client)
        {
            this.client = client ?? throw new ArgumentNullException(nameof(client));
        }

        public async Task<IEnumerable<string>> GetDataTypesAsync()
        {
            var response = await client.EditorDatatypesAsync();
            return response.Items;
        }
    }
}
