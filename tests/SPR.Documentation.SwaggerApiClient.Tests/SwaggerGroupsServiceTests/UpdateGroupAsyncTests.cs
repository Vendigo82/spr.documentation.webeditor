﻿using AutoFixture.Xunit2;
using AutoMapper;
using FluentAssertions;
using Moq;
using RichardSzalay.MockHttp;
using SPR.Documentation.WebEditor.Exceptions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Json;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace SPR.Documentation.SwaggerApiClient.SwaggerGroupsServiceTests
{
    public class UpdateGroupAsyncTests
    {
        protected readonly MockHttpMessageHandler mockHandler = new();
        protected readonly SwaggerGroupsService target;

        public UpdateGroupAsyncTests()
        {

            var mapper = new Mapper(new MapperConfiguration(c => c.AddProfile<MappingProfile>()));
            var httpClient = mockHandler.ToHttpClient();
            httpClient.BaseAddress = new Uri("http://localhost");
            target = new(new EditorClient(Mock.Of<ITokenProvider>(), httpClient), mapper);
        }

        [Theory, AutoData]
        public async Task ShouldBeSuccess(WebEditor.Models.GroupModel model)
        {
            // setup
            //string content = null;
            var request = mockHandler
                .When(HttpMethod.Post, $"/editor/groups/{model.Id}")
                .WithPartialContent($"\"id\":\"{model.Id}\"")
                .WithPartialContent($"\"alias\":\"{model.Alias}\"")
                .Respond(HttpStatusCode.OK);

            // action
            await target.UpdateGroupAsync(model);

            // asserts
            mockHandler.GetMatchCount(request).Should().Be(1);
        }

        [Theory, AutoData]
        public async Task RespondAliasConflict_ShouldBeModelValidationException(WebEditor.Models.GroupModel model, string detail)
        {
            // setup
            mockHandler
                .When(HttpMethod.Post, $"/editor/groups/{model.Id}")
                .Respond(HttpStatusCode.Conflict, JsonContent.Create(new {
                    title = "Group already exists",
                    detail
                }));

            // action
            Func<Task> action = () => target.UpdateGroupAsync(model);

            // asserts
            var item = (await action.Should().ThrowExactlyAsync<ModelValidationException>()).Which.ValidationErrors.Should().ContainSingle().Which;
            item.Key.Should().Be(nameof(model.Alias));
            item.Value.Should().ContainSingle().Which.Should().BeEquivalentTo(detail);
        }
    }
}
