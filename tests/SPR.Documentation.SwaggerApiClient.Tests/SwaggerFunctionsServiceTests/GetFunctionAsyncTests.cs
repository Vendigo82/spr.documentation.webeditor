﻿using AutoFixture.Xunit2;
using FluentAssertions;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace SPR.Documentation.SwaggerApiClient.Tests.SwaggerFunctionsServiceTests
{
    public class GetFunctionAsyncTests : SwaggerFunctionsServiceBaseTests
    {
        [Theory, AutoData]
        public async Task SuccessTest(FunctionModel responseData)
        {
            // setup
            clientMock.Setup(f => f.EditorFunctionsGetAsync(responseData.Id, default)).ReturnsAsync(responseData);

            // action
            var result = await target.GetFunctionAsync(responseData.Id);

            // asserts
            result.Should().NotBeNull().And.BeEquivalentTo(responseData);
        }

        [Theory, AutoData]
        public async Task ResponseNoContent_ShouldReturnNull_Test(Guid functionId)
        {
            // setup
            clientMock.Setup(f => f.EditorFunctionsGetAsync(functionId, default)).ThrowsAsync(new ApiException("", 204, "", null, null));

            // action
            var result = await target.GetFunctionAsync(functionId);

            // asserts
            result.Should().BeNull();
        }
    }
}
