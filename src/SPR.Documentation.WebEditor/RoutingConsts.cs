﻿namespace SPR.Documentation.WebEditor
{
    public static class RoutingConsts
    {
        public const string LoginUrl = "/login";
        public const string RedirectUrlQueryParam = "redirectUrl";
    }
}
