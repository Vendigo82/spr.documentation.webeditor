﻿using SPR.Documentation.WebEditor.Exceptions;
using System.Collections.Generic;
using System.Linq;

namespace SPR.Documentation.SwaggerApiClient
{
    public static class ExceptionExtensions
    {
        public static ModelValidationException ToValidationException(this ApiException<ValidationProblemDetails> e)
        {
            return new ModelValidationException(e.Result.Errors!.Select(i => new KeyValuePair<string, IEnumerable<string>>(i.Key, i.Value)));
        }
    }
}
