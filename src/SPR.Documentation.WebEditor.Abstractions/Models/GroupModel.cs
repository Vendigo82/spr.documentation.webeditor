﻿using System.Collections.Generic;

namespace SPR.Documentation.WebEditor.Models
{
    public class GroupModel : GroupBriefModel
    {
        /// <summary>
        /// Group's titles on different languages
        /// </summary>
        public ICollection<TextResource> Titles { get; set; } = new List<TextResource>();
    }
}
