﻿using AutoFixture.Xunit2;
using Bunit;
using FluentAssertions;
using Microsoft.Extensions.DependencyInjection;
using Moq;
using SPR.Documentation.WebEditor.Abstractions;
using SPR.Documentation.WebEditor.Models;
using SPR.Documentation.WebEditor.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace SPR.Documentation.WebEditor.Pages.Groups
{
    public class GroupsIndexTests : TestContext
    {
        readonly Mock<IGroupsService> serviceMock = new();
        readonly Mock<ITokenContainer> tokenContainerMock = new();

        public GroupsIndexTests()
        {
            Services.AddSingleton(serviceMock.Object);
            Services.AddSingleton(tokenContainerMock.Object);
        }

        [Theory, AutoData]
        public void InitialStateShouldDisplayFunctionsTest(IEnumerable<GroupBriefModel> items)
        {
            // setup
            serviceMock.Setup(f => f.GetGroupsAsync()).ReturnsAsync(items);            

            // action
            var cut = RenderComponent<GroupsIndex>();

            // asserts            
            var refs = cut.Find("table").GetElementsByTagName("a").ToList();
            refs.Select(i => i.GetAttribute("href")).Should().Equal(items.Select(i => $"/groups/edit/{i.Id}"));
            refs.Select(i => i.InnerHtml).Should().Equal(items.Select(i => $"{i.Alias}"));

            serviceMock.Verify(f => f.GetGroupsAsync(), Times.Once);
            serviceMock.VerifyNoOtherCalls();
        }

        [Fact]
        public async Task InitialLoadingTextShouldDissapear()
        {
            // setup
            serviceMock.Setup(f => f.GetGroupsAsync()).ReturnsAsync(Enumerable.Empty<GroupBriefModel>(), TimeSpan.FromMilliseconds(100));

            // action
            var cut = RenderComponent<GroupsIndex>();

            // asserts
            cut.Markup.Should().Contain("Loading");
            await Task.Delay(200);
            cut.Markup.Should().NotContain("Loading");
        }
    }
}
