﻿using SPR.Documentation.WebEditor.Abstractions;
using System;

namespace SPR.Documentation.WebEditor.Services
{
    public class ApiServicesAggregator
    {
        public ApiServicesAggregator(IFunctionsService functions, IDictionarySource dictionary)
        {
            Functions = functions ?? throw new ArgumentNullException(nameof(functions));
            Dictionary = dictionary ?? throw new ArgumentNullException(nameof(dictionary));
        }

        public IFunctionsService Functions { get; }

        public IDictionarySource Dictionary { get; }
    }
}
