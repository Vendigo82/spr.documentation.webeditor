﻿using System;
using System.Collections.Generic;

namespace SPR.Documentation.WebEditor.Models
{
    /// <summary>
    /// Function model
    /// </summary>
    public class FunctionModel : FunctionBriefModel
    {
        /// <summary>
        /// Function titles on different languages
        /// </summary>
        public ICollection<TextResource> Titles { get; set; } = new List<TextResource>();

        /// <summary>
        /// Function descriptions on different languages
        /// </summary>
        public ICollection<TextResource> Descriptions { get; set; } = new List<TextResource>();

        /// <summary>
        /// List of arguments
        /// </summary>
        public ICollection<ArgumentModel> Arguments { get; set; } = new List<ArgumentModel>();

        /// <summary>
        /// Returns value
        /// </summary>
        public ReturnValueModel? Return { get; set; }

        /// <summary>
        /// Library version which function appears
        /// </summary>
        public string? LibraryVersion { get; set; }

        /// <summary>
        /// List of groups
        /// </summary>
        public ICollection<Guid>? Groups { get; set; }
    }
}
