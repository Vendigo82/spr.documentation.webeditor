#See https://aka.ms/containerfastmode to understand how Visual Studio uses this Dockerfile to build your images for faster debugging.

#Depending on the operating system of the host machines(s) that will build or run the containers, the image specified in the FROM statement may need to be changed.
#For more information, please see https://aka.ms/containercompat

FROM mcr.microsoft.com/dotnet/aspnet:6.0 AS base
WORKDIR /app
EXPOSE 80
EXPOSE 443

FROM mcr.microsoft.com/dotnet/sdk:6.0 AS build
WORKDIR /src
#COPY ["SPR.Documentation.WebService/SPR.Documentation.WebService.csproj", "SPR.Documentation.WebService/"]
#RUN dotnet restore "SPR.Documentation.WebService/SPR.Documentation.WebService.csproj"
COPY ./src .
WORKDIR "/src/SPR.Documentation.WebEditor"
RUN dotnet build "SPR.Documentation.WebEditor.csproj" -c Release -o /app/build

FROM build AS publish
ARG buildno=0-test
RUN dotnet publish "SPR.Documentation.WebEditor.csproj" -c Release -o /app/publish --version-suffix $buildno

FROM base AS final
WORKDIR /app
COPY --from=publish /app/publish .
ENTRYPOINT ["dotnet", "SPR.Documentation.WebEditor.dll"]