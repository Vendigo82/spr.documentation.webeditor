﻿using IdentityModel.Client;
using Microsoft.Extensions.Logging;
using SPR.Documentation.SwaggerApiClient;
using System;
using System.Net.Http;
using System.Threading.Tasks;

namespace SPR.Documentation.WebEditor.Services
{
    public class TokenProviderBase
    {
        public string? AccessToken { get; set; }

        public string? RefreshToken { get; set; }

        public DateTimeOffset? ExpiresAt { get; set; }

        /// <summary>
        /// Cookie experation
        /// </summary>
        public DateTimeOffset? Expiration { get; set; }

        public string? SubjectId { get; set; }
    }

    public interface ITokenContainer
    {
        public TokenProviderBase? Token { get; }

        public void Assign(TokenProviderBase tokenProvider);
    }

    public class TokenProvider : /*TokenProviderBase,*/ ITokenContainer, ITokenProvider
    {
        //public string? AccessToken { get; set; }

        //public string? RefreshToken { get; set; }

        public TokenProviderBase? Token { get; private set; }

        private readonly IHttpClientFactory httpClientFactory;
        private readonly IDiscoveryCache discoveryCache;
        private readonly ILogger<TokenProvider> logger;


        public TokenProvider(IHttpClientFactory httpClientFactory, IDiscoveryCache discoveryCache, ILogger<TokenProvider> logger)
        {
            this.discoveryCache = discoveryCache ?? throw new ArgumentNullException(nameof(discoveryCache));
            this.httpClientFactory = httpClientFactory ?? throw new ArgumentNullException(nameof(httpClientFactory));
            this.logger = logger ?? throw new ArgumentNullException(nameof(logger));
        }

        public void Assign(TokenProviderBase tokenProvider)
        {
            //AccessToken = tokenProvider.AccessToken;
            //RefreshToken = tokenProvider.RefreshToken;
            //ExpiresAt = tokenProvider.ExpiresAt;
            //Expiration = tokenProvider.Expiration;
            //SubjectId = tokenProvider.SubjectId;            
            Token = tokenProvider;
        }

        public async Task<string?> GetAccessTokenAsync()
        {
            if (Token != null && Token.ExpiresAt != null && Token.RefreshToken != null)
            {
                var span = Token.ExpiresAt.Value - DateTime.UtcNow;
                logger.LogTrace("Token life interval ({})", span);
                if (span < TimeSpan.Zero)
                {
                    logger.LogDebug("Token was expired ({})", span);
                    var client = httpClientFactory.CreateClient("is-client");
                    var discovery = await discoveryCache.GetAsync();
                    logger.LogDebug("Discovery document: {@discovery}", discovery);

                    if (!discovery.IsError)
                    {
                        var requestTime = DateTime.UtcNow;
                        var response = await client.RequestRefreshTokenAsync(new RefreshTokenRequest {
                            Address = discovery.TokenEndpoint,
                            ClientId = "spr.documentation",
                            ClientSecret = "meripoppins",
                            RefreshToken = Token.RefreshToken
                        });

                        if (!response.IsError)
                        {
                            lock (Token)
                            {
                                logger.LogDebug("Token was refreshed. Expires In {ExpiresIn}", response.ExpiresIn);
                                Token.AccessToken = response.AccessToken;
                                Token.RefreshToken = response.RefreshToken;
                                Token.ExpiresAt = requestTime + TimeSpan.FromSeconds(response.ExpiresIn);
                            }
                        }
                        else
                        {
                            logger.LogWarning(response.Exception, "Refresh error of type {ErrorType}: {Error} {ErrorDescription}", response.ErrorType, response.Error, response.ErrorDescription);
                        }
                    } 
                    else
                    {
                        logger.LogError(discovery.Exception, "Discovery document error {ErrorType}: {ErrorDescription}", discovery.ErrorType, discovery.Error);
                    }
                }
            }
            
            return Token?.AccessToken;
        }
    }

    public class InitialTokenProvider : TokenProviderBase
    {
    }
}
