﻿using AutoFixture.Xunit2;
using FluentAssertions;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace SPR.Documentation.SwaggerApiClient.Tests.SwaggerFunctionsServiceTests
{
    public class GetListAsyncTests : SwaggerFunctionsServiceBaseTests
    {
        [Theory]
        [InlineAutoData(null, null, null)]
        [InlineAutoData]
        public async Task SuccessTest(string filter, int? maxCount, string token, FunctionBriefModelPartialItemsContainer response)
        {
            // setup
            clientMock.Setup(f => f.EditorFunctionsGetAsync(filter, null, maxCount, token, default)).ReturnsAsync(response);

            // action
            var (resultItems, resultToken) = await target.GetListAsync(token, filter, maxCount);

            // asserts
            resultItems.Should().BeEquivalentTo(response.Items);
            resultToken.Should().Be(response.NextToken);

            clientMock.Verify(f => f.EditorFunctionsGetAsync(filter, null, maxCount, token, default), Times.Once);
            clientMock.VerifyNoOtherCalls();
        }

        [Theory]
        [InlineAutoData(null, null, null)]
        [InlineAutoData]
        public async Task SuccessWithOffsetTest(string filter, int? maxCount, int? offset, FunctionBriefModelPartialItemsContainer response)
        {
            // setup
            clientMock.Setup(f => f.EditorFunctionsGetAsync(filter, offset, maxCount, null, default)).ReturnsAsync(response);

            // action
            var (resultItems, resultToken) = await target.GetListAsync(filter, offset, maxCount);

            // asserts
            resultItems.Should().BeEquivalentTo(response.Items);
            resultToken.Should().Be(response.Exhausted);

            clientMock.Verify(f => f.EditorFunctionsGetAsync(filter, offset, maxCount, null, default), Times.Once);
            clientMock.VerifyNoOtherCalls();
        }
    }
}
