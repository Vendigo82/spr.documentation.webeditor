﻿using AutoFixture.Xunit2;
using Bunit;
using FluentAssertions;
using Microsoft.AspNetCore.Components.Forms;
using SPR.Documentation.WebEditor.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace SPR.Documentation.WebEditor.Components
{
    public class TextResourceListTests : TestContext
    {
        [Theory, AutoData]
        public void ValidateListElementsTest(ICollection<TextResource> texts, string caption)
        {
            // setup && action
            var cut = RenderComponent<TextResourcesList>(parameters => parameters
                .Add(p => p.Items, texts)
                .Add(p => p.Caption, caption)
                .Add(p => p.ItemTemplate, item => $"<span>{item.Text}</span>")        
                .AddCascadingValue(new EditContext(new object()))
            );

            // asserts
            cut.FindAll("input").Select(i => i.GetAttribute("value")).Should().BeEquivalentTo(texts.Select(i => i.Lang));
            cut.FindAll("span").Select(i => i.InnerHtml).Should().BeEquivalentTo(texts.Select(i => i.Text));
        }

        [Theory, AutoData]
        public void ValidateHeaderMarkup(string caption)
        {
            // setup && action
            var cut = RenderComponent<TextResourcesList>(parameters => parameters
                .Add(p => p.Items, Enumerable.Empty<TextResource>().ToList())
                .Add(p => p.Caption, caption)
                .Add(p => p.ItemTemplate, item => $"<span>{item.Text}</span>")
                .AddCascadingValue(new EditContext(new object()))
            );

            // asserts
            cut.Find("thead").QuerySelectorAll("th").Select(i => i.InnerHtml).Should().Equal(new[] { "", caption });
        }
    }
}
