﻿using AngleSharp.Dom;
using AutoFixture.Xunit2;
using BootstrapBlazor.Components;
using Bunit;
using FluentAssertions;
using Microsoft.AspNetCore.Components.Forms;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Localization;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Moq;
using SPR.Documentation.WebEditor.Abstractions;
using SPR.Documentation.WebEditor.Components;
using SPR.Documentation.WebEditor.Models;
using SPR.Documentation.WebEditor.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace SPR.Documentation.WebEditor.Pages.Functions
{
    public class FunctionEditTests : TestContext
    {
        readonly Mock<IFunctionsService> serviceMock = new();
        readonly Mock<IDictionarySource> dictionarySourceMock = new();
        readonly Mock<IGroupsService> groupsServiceMock = new();
        readonly Mock<ITokenContainer> tokenContainerMock = new();
        readonly Mock<MessageService> messageServiceMock;

        public FunctionEditTests()
        {
            Services.AddSingleton(serviceMock.Object);
            Services.AddSingleton(tokenContainerMock.Object);
            Services.AddSingleton(dictionarySourceMock.Object);
            Services.AddSingleton(groupsServiceMock.Object);
            Services.AddSingleton<FunctionEditForm.ServiceBunch>();
            messageServiceMock = new Mock<MessageService>(() => new MessageService(Mock.Of<IOptionsMonitor<BootstrapBlazorOptions>>(), Mock.Of<IStringLocalizer<MessageService>>()));
            Services.AddSingleton(messageServiceMock.Object);
            Services.AddSingleton(Mock.Of<ILogger<FunctionEdit>>());

            dictionarySourceMock.Setup(f => f.GetDataTypesAsync()).ReturnsAsync(new string[] { "String", "Integer" });
        }

        [Fact]
        public void InitialLoadForNewFuntionTest()
        {
            // setup            

            // action
            var cut = RenderComponent<FunctionEdit>();

            // asserts
            cut.RenderCount.Should().Be(1);

            cut.Find($"#{nameof(FunctionModel.ArgsCount)}").GetAttribute("value").Should().Be("0");
            cut.Find($"#{nameof(FunctionModel.LibraryVersion)}").GetAttribute("value").Should().Be("1.0.0");

            cut.Instance.InputModel.Return.Type.Should().NotBeNull().And.Be("String");

            ValidateFunctionForm(cut, cut.Instance.InputModel);
        }

        [Theory, AutoData]
        public void InitialLoadForExistingFunctionTest(FunctionModel model)
        {
            // setup            
            model.ArgsCount = model.Arguments.Count;
            serviceMock.Setup(f => f.GetFunctionAsync(model.Id)).ReturnsAsync(model);

            // action
            var cut = RenderComponent<FunctionEdit>(ComponentParameter.CreateParameter(nameof(FunctionEdit.Id), model.Id.ToString()));

            // asserts
            cut.RenderCount.Should().Be(1);

            ValidateFunctionForm(cut, model);
        }

        [Theory, AutoData]
        public void SaveNewFunctionTest(Guid newFunctionId)
        {
            // setup            
            serviceMock.Setup(f => f.FunctionNewAsync(It.IsAny<FunctionModel>())).ReturnsAsync(newFunctionId);
            var cut = RenderComponent<FunctionEdit>();

            // action
            cut.Find($"#{nameof(FunctionModel.SystemName)}").Change("newvalue");
            cut.Find("form").Submit();

            // asserts
            serviceMock.Verify(f => f.FunctionNewAsync(cut.Instance.InputModel), Times.Once);
            cut.Instance.Id.Should().Be(newFunctionId.ToString());

            messageServiceMock.Verify(f => f.Show(It.Is<MessageOption>(o => o.Color == Color.Success && o.Content == "Function was saved")));
        }

        [Theory, AutoData]
        public void EmptyLibraryVersionShouldBeAssignedToNullOnSaveTest(Guid newFunctionId)
        {
            // setup            
            serviceMock.Setup(f => f.FunctionNewAsync(It.IsAny<FunctionModel>())).ReturnsAsync(newFunctionId);
            var cut = RenderComponent<FunctionEdit>();

            // action
            cut.Find($"#{nameof(FunctionModel.SystemName)}").Change("newvalue");
            cut.Find($"#{nameof(FunctionModel.LibraryVersion)}").Change("");
            cut.Find("form").Submit();

            // asserts            
            serviceMock.Verify(f => f.FunctionNewAsync(It.Is<FunctionModel>(m => m.LibraryVersion == null)), Times.Once);            
        }

        [Theory, AutoData]
        public void UpdateExistedFunction(FunctionModel model)
        {
            // setup            
            model.ArgsCount = model.Arguments.Count;
            serviceMock.Setup(f => f.GetFunctionAsync(model.Id)).ReturnsAsync(model);

            var cut = RenderComponent<FunctionEdit>(ComponentParameter.CreateParameter(nameof(FunctionEdit.Id), model.Id.ToString()));

            // action
            cut.Find($"#{nameof(FunctionModel.SystemName)}").Change("newvalue");
            cut.Find("form").Submit();

            // asserts
            serviceMock.Verify(f => f.FunctionUpdateAsync(cut.Instance.InputModel), Times.Once);

            messageServiceMock.Verify(f => f.Show(It.Is<MessageOption>(o => o.Color == Color.Success && o.Content == "Function was saved")));
        }

        [Theory]
        [InlineData(0)]
        [InlineData(1)]
        [InlineData(5)]
        public void ArgumentsTableShouldChangeRowsCount(int argsCount)
        {
            // setup
            var cut = RenderComponent<FunctionEdit>();

            // action
            var argsCountInput = cut.Find($"#{nameof(FunctionModel.ArgsCount)}");            
            argsCountInput.Change(argsCount);
            cut.Instance.InputModel.ArgsCount = argsCount;  // does not changed by himself
            argsCountInput.Input(argsCount.ToString());            
            cut.Render();

            // asserts
            cut.Instance.InputModel.Arguments.Should().HaveCount(argsCount);

            var argsRows = cut.Find($"#ArgumentsTable").QuerySelectorAll("tr");
            argsRows.Should().HaveCount(argsCount + 1);
        }

        [Theory, AutoData]
        public void DataTypesShouldBeFilled(IEnumerable<string> dataTypes, FunctionModel model)
        {
            // setup            
            model.ArgsCount = model.Arguments.Count;
            dictionarySourceMock.Setup(f => f.GetDataTypesAsync()).ReturnsAsync(dataTypes);
            serviceMock.Setup(f => f.GetFunctionAsync(model.Id)).ReturnsAsync(model);

            // action
            var cut = RenderComponent<FunctionEdit>(ComponentParameter.CreateParameter(nameof(FunctionEdit.Id), model.Id.ToString()));

            // asserts
            var selects = cut.FindAll("select");
            selects.Should().NotBeEmpty();
            foreach (var selectItem in selects)
                selectItem.QuerySelectorAll("option").Select(i => i.InnerHtml).Should().Equal(dataTypes);
        }

        [Theory, AutoData]
        public void FunctionNotFoundTest(Guid functionId)
        {
            // setup
            serviceMock.Setup(f => f.GetFunctionAsync(functionId)).ReturnsAsync((FunctionModel)null);

            // action
            var cut = RenderComponent<FunctionEdit>(ComponentParameter.CreateParameter(nameof(FunctionEdit.Id), functionId.ToString()));

            // asserts
            cut.MarkupMatches("<p><em>Sorry, function not found</em></p>");
        }

        [Theory, AutoData]
        public void SaveNewFunction_ThrowsValidationException_ShouldBeValidationErrorTest(string errorMessage)
        {
            // setup            
            serviceMock.Setup(f => f.FunctionNewAsync(It.IsAny<FunctionModel>())).ThrowsAsync(new Exceptions.ModelValidationException("LibraryVersion", errorMessage));
            var cut = RenderComponent<FunctionEdit>();

            // action
            cut.Find($"#{nameof(FunctionModel.SystemName)}").Change("newvalue");
            cut.Find("form").Submit();

            // asserts
            serviceMock.Verify(f => f.FunctionNewAsync(cut.Instance.InputModel), Times.Once);
            cut.Instance.Id.Should().BeNull();

            cut.Find("#LibraryVersion").IsValid().Should().BeFalse();
        }

        [Theory, AutoData]
        public void UpdateExistedFunction_ThrowsValidationException_ShouldBeValidationErrorTest(FunctionModel model, string errorMessage)
        {
            // setup            
            model.ArgsCount = model.Arguments.Count;
            serviceMock.Setup(f => f.GetFunctionAsync(model.Id)).ReturnsAsync(model);
            serviceMock.Setup(f => f.FunctionUpdateAsync(model)).ThrowsAsync(new Exceptions.ModelValidationException("LibraryVersion", errorMessage));

            var cut = RenderComponent<FunctionEdit>(ComponentParameter.CreateParameter(nameof(FunctionEdit.Id), model.Id.ToString()));

            // action
            cut.Find($"#{nameof(FunctionModel.SystemName)}").Change("newvalue");
            cut.Find("form").Submit();

            // asserts
            serviceMock.Verify(f => f.FunctionUpdateAsync(cut.Instance.InputModel), Times.Once);
            cut.Find("#LibraryVersion").IsValid().Should().BeFalse();
        }

        [Theory, AutoData]
        public void SaveNewFunction_ThrowsException_ShouldBeErrorMessageTest(string errorMessage)
        {
            // setup            
            serviceMock.Setup(f => f.FunctionNewAsync(It.IsAny<FunctionModel>()))
                .ThrowsAsync(new Exception(errorMessage));
            var cut = RenderComponent<FunctionEdit>();

            // action
            cut.Find($"#{nameof(FunctionModel.SystemName)}").Change("newvalue");
            cut.Find("form").Submit();

            // asserts
            serviceMock.Verify(f => f.FunctionNewAsync(cut.Instance.InputModel), Times.Once);
            cut.Instance.Id.Should().BeNull();
            
            messageServiceMock.Verify(f => f.Show(It.Is<MessageOption>(o => o.Color == Color.Danger && o.Content.Contains(errorMessage))));
        }

        [Theory, AutoData]
        public void UpdateExistedFunction_ThrowsException_ShouldBeErrorMessageTest(FunctionModel model, string errorMessage)
        {
            // setup            
            model.ArgsCount = model.Arguments.Count;
            serviceMock.Setup(f => f.GetFunctionAsync(model.Id)).ReturnsAsync(model);
            serviceMock.Setup(f => f.FunctionUpdateAsync(model))
                .ThrowsAsync(new Exception(errorMessage));

            var cut = RenderComponent<FunctionEdit>(ComponentParameter.CreateParameter(nameof(FunctionEdit.Id), model.Id.ToString()));

            // action
            cut.Find($"#{nameof(FunctionModel.SystemName)}").Change("newvalue");
            cut.Find("form").Submit();

            // asserts
            serviceMock.Verify(f => f.FunctionUpdateAsync(cut.Instance.InputModel), Times.Once);

            messageServiceMock.Verify(f => f.Show(It.Is<MessageOption>(o => o.Color == Color.Danger && o.Content.Contains(errorMessage))));
        }

        [Theory, AutoData]
        public void AllGroupsShouldBeDisplayed(FunctionModel model, IEnumerable<GroupBriefModel> groups)
        {
            // setup
            model.ArgsCount = model.Arguments.Count;
            
            serviceMock.Setup(f => f.GetFunctionAsync(model.Id)).ReturnsAsync(model);
            groupsServiceMock.Setup(f => f.GetGroupsAsync()).ReturnsAsync(groups);

            // action
            var cut = RenderComponent<FunctionEdit>(ComponentParameter.CreateParameter(nameof(FunctionEdit.Id), model.Id.ToString()));

            // asserts
            foreach (var g in groups)
            {
                var element = cut.Find($"#group{g.Id}");
                element.Should().NotBeNull();
                element.NextElementSibling.InnerHtml.Should().Be(g.Alias);
            }
        }

        //[Theory]
        //[InlineAutoData(true)]
        //[InlineAutoData(false)]
        //public void LoadExistingFunction_GroupInitialSelectionTest(bool selected, FunctionModel model, GroupBriefModel group)
        //{
        //    // setup
        //    model.ArgsCount = model.Arguments.Count;
        //    model.Groups = selected ? new[] { group.Id } : Array.Empty<Guid>();

        //    serviceMock.Setup(f => f.GetFunctionAsync(model.Id)).ReturnsAsync(model);
        //    groupsServiceMock.Setup(f => f.GetGroupsAsync())
        //        .ReturnsAsync(new[] { group });

        //    // action
        //    var cut = RenderComponent<FunctionEdit>(ComponentParameter.CreateParameter(nameof(FunctionEdit.Id), model.Id.ToString()));
        //    cut.Render();

        //    // asserts            
        //    cut.Find($"#group{group.Id}").IsChecked().Should().Be(selected);
        //}

        private static void ValidateFunctionForm(IRenderedFragment cut, FunctionModel model) 
        {
            cut.Find($"#{nameof(FunctionModel.SystemName)}").GetAttribute("value").Should().Be(model.SystemName);
            cut.Find($"#{nameof(FunctionModel.ArgsCount)}").GetAttribute("value").Should().Be(model.ArgsCount.ToString());
            //if (model.Return != null)
            //    cut.Find($"#withReturnValue").IsChecked().Should().Be(model.Return != null);//.GetAttribute("checked").Should().BeEmpty();
            //else
                //cut.Find($"#withReturnValue").GetAttribute("checked").Should().BeNull();
            cut.Find($"#{nameof(FunctionModel.LibraryVersion)}").GetAttribute("value").Should().Be(model.LibraryVersion);

            var texts = cut.FindComponents<TextResourcesList>();

            texts.Should().ContainSingle(i => i.Instance.Items == model.Titles);
            texts.Should().ContainSingle(i => i.Instance.Items == model.Descriptions);

            var argsRows = cut.Find($"#ArgumentsTable").QuerySelectorAll("tr");
            argsRows.Should().HaveCount(model.Arguments.Count + 1);
        }
    }
}
