﻿using AutoFixture.Xunit2;
using AutoMapper;
using FluentAssertions;
using Moq;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using RichardSzalay.MockHttp;
using SPR.Documentation.WebEditor.Exceptions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Json;
using System.Text;
using System.Text.Json;
using System.Threading.Tasks;
using Xunit;

namespace SPR.Documentation.SwaggerApiClient.SwaggerGroupsServiceTests
{
    public class CreateGroupAsyncTests
    {
        protected readonly MockHttpMessageHandler mockHandler = new();
        protected readonly SwaggerGroupsService target;

        public CreateGroupAsyncTests()
        {

            var mapper = new Mapper(new MapperConfiguration(c => c.AddProfile<MappingProfile>()));
            var httpClient = mockHandler.ToHttpClient();
            httpClient.BaseAddress = new Uri("http://localhost");
            target = new(new EditorClient(Mock.Of<ITokenProvider>(), httpClient), mapper);
        }

        [Theory, AutoData]
        public async Task ShouldBeSuccess(WebEditor.Models.GroupModel model)
        {
            // setup
            //string content = null;
            mockHandler
                .When(HttpMethod.Put, "/editor/groups")
                .WithPartialContent($"\"id\":\"{model.Id}\"")
                .WithPartialContent($"\"alias\":\"{model.Alias}\"")
                //.With(m => { content = m.Content.ReadAsStringAsync().Result; return true; })
                .Respond(HttpStatusCode.Created, JsonContent.Create(new { id = model.Id }));

            // action
            var result = await target.CreateGroupAsync(model);

            // asserts
            result.Should().Be(model.Id);
        }

        [Theory, AutoData]
        public async Task RespondIdConflict_ShouldBeSuccess(WebEditor.Models.GroupModel model)
        {
            // setup
            mockHandler
                .When(HttpMethod.Put, "/editor/groups")
                .Respond(HttpStatusCode.Conflict, JsonContent.Create(new {
                    title = "Primary key conflict",
                    detail = ""
                }));

            // action
            var result = await target.CreateGroupAsync(model);

            // asserts
            result.Should().Be(model.Id);
        }

        [Theory, AutoData]
        public async Task RespondAliasConflict_ShouldBeModelValidationException(WebEditor.Models.GroupModel model, string detail)
        {
            // setup
            mockHandler
                .When(HttpMethod.Put, "/editor/groups")
                .Respond(HttpStatusCode.Conflict, JsonContent.Create(new {
                    title = "Group already exists",
                    detail
                }));

            // action
            Func<Task> action = () => target.CreateGroupAsync(model);

            // asserts
            var item = (await action.Should().ThrowExactlyAsync<ModelValidationException>()).Which.ValidationErrors.Should().ContainSingle().Which;
            item.Key.Should().Be(nameof(model.Alias));
            item.Value.Should().ContainSingle().Which.Should().BeEquivalentTo(detail);
        }
    }
}
