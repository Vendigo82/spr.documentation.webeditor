﻿using AutoMapper;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SPR.Documentation.SwaggerApiClient.SwaggerGroupsServiceTests
{
    public class SwaggerGroupsServiceBaseTests
    {
        protected SwaggerGroupsService target;
        protected Mock<IEditorClient> clientMock = new();

        public SwaggerGroupsServiceBaseTests()
        {
            var mapper = new Mapper(new MapperConfiguration(c => c.AddProfile<MappingProfile>()));
            target = new(clientMock.Object, mapper);
        }
    }
}
