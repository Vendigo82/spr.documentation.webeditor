﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace SPR.Documentation.WebEditor.Abstractions
{
    public interface IDictionarySource
    {
        /// <summary>
        /// Get list of available data types
        /// </summary>
        /// <returns></returns>
        public Task<IEnumerable<string>> GetDataTypesAsync();
    }
}
