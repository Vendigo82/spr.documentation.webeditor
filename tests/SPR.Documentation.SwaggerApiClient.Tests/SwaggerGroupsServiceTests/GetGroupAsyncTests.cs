﻿using AutoFixture.Xunit2;
using FluentAssertions;
using Microsoft.AspNetCore.Http;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace SPR.Documentation.SwaggerApiClient.SwaggerGroupsServiceTests
{
    public class GetGroupAsyncTests : SwaggerGroupsServiceBaseTests
    {
        [Theory, AutoData]
        public async Task SuccessTest(GroupModel model)
        {
            // setup
            clientMock.Setup(f => f.EditorGroupsGetAsync(model.Id, default)).ReturnsAsync(model);

            // action
            var result = await target.GetGroupAsync(model.Id);

            // asserts
            result.Should().NotBeNull().And.BeEquivalentTo(model);
            clientMock.Verify(f => f.EditorGroupsGetAsync(model.Id, default), Times.Once);
        }

        [Theory, AutoData]
        public async Task NoContent_ShouldReturnsNullTest(Guid id)
        {
            // setup
            clientMock.Setup(f => f.EditorGroupsGetAsync(id, default))
                .ThrowsAsync(new ApiException("", StatusCodes.Status204NoContent, null, null, null));

            // action
            var result = await target.GetGroupAsync(id);

            // asserts
            result.Should().BeNull();
        }
    }
}
