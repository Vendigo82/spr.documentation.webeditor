﻿using AutoMapper;
using Microsoft.AspNetCore.Http;
using SPR.Documentation.WebEditor.Abstractions;
using SPR.Documentation.WebEditor.Exceptions;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace SPR.Documentation.SwaggerApiClient
{
    public class SwaggerGroupsService : IGroupsService
    {
        private readonly IEditorClient client;
        private readonly IMapper mapper;

        public SwaggerGroupsService(IEditorClient client, IMapper mapper)
        {
            this.client = client ?? throw new ArgumentNullException(nameof(client));
            this.mapper = mapper ?? throw new ArgumentNullException(nameof(mapper));
        }

        public async Task<IEnumerable<WebEditor.Models.GroupBriefModel>> GetGroupsAsync()
        {
            var response = await client.EditorGroupsGetAsync();
            var result = mapper.Map<IEnumerable<WebEditor.Models.GroupBriefModel>>(response.Items);
            return result;
        }

        public async Task<WebEditor.Models.GroupModel?> GetGroupAsync(Guid id)
        {
            try
            {
                var response = await client.EditorGroupsGetAsync(id);
                var result = mapper.Map<WebEditor.Models.GroupModel>(response);
                return result;
            }
            catch (ApiException e) when (e.StatusCode == StatusCodes.Status204NoContent)
            {
                return null;
            }
        }

        public async Task<Guid> CreateGroupAsync(WebEditor.Models.GroupModel model)
        {
            try
            {
                var request = mapper.Map<GroupModel>(model);
                var response = await client.EditorGroupsPutAsync(request);
                return response.Id;
            } 
            catch (ApiException<ProblemDetails> e) when (e.StatusCode == StatusCodes.Status409Conflict && e.Result.Title == "Group already exists")
            {
                throw new ModelValidationException(nameof(model.Alias), e.Result.Detail!);
            }
            catch (ApiException<ProblemDetails> e) when (e.StatusCode == StatusCodes.Status409Conflict && e.Result.Title == "Primary key conflict")
            {
                return model.Id;
            }
            catch (ApiException<ValidationProblemDetails> e) when (e.StatusCode == StatusCodes.Status422UnprocessableEntity && e.Result.Errors != null)
            {
                throw e.ToValidationException();
            }
        }

        public async Task UpdateGroupAsync(WebEditor.Models.GroupModel model)
        {
            try
            {
                var request = mapper.Map<GroupModel>(model);
                await client.EditorGroupsPostAsync(model.Id, request);
            }
            catch (ApiException<ProblemDetails> e) when (e.StatusCode == StatusCodes.Status409Conflict && e.Result.Title == "Group already exists")
            {
                throw new ModelValidationException(nameof(model.Alias), e.Result.Detail!);
            }
            catch (ApiException<ValidationProblemDetails> e) when (e.StatusCode == StatusCodes.Status422UnprocessableEntity && e.Result.Errors != null)
            {
                throw e.ToValidationException();
            }
        }
    }
}
